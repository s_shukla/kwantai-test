import json, os, requests


def post_reports_to_slack():
    # To generate report file add "> pytest_report.log" at end
    test_report_file = os.path.abspath(
        os.path.join(os.path.dirname(__file__), 'var/pytest_report.log')
    )

    # Open report file and read data
    with open(test_report_file, "r") as in_file:
        test_data = ""
        for line in in_file:
            test_data = test_data + '\n' + line

    # Slack Pass Fail bar indicator color according to test results
    if ('PASSED' and '.' and 'Model accuracy = 100.0%') in test_data:
        bar_color = "#36a64f"
    else:
        bar_color = "#ff0000"

    # Arrange data in pre-defined format.
    data = {
        "attachments": [
            {
                "color": bar_color,
                "title": "Test Report",
                "text": test_data
             }
        ]
    }
    json_params_encoded = json.dumps(data)
    slack_response = requests.post(

        # Test slack channel '#kwantai-test' webhook url
        url="https://hooks.slack.com/services/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",
        data=json_params_encoded,
        headers={"Content-type": "application/json"}
    )
    if slack_response.text == 'ok':
        print('\n Successfully posted pytest report on Slack channel')
    else:
        print('\n Something went wrong. Slack Response:', slack_response)


if __name__ == '__main__':
    post_reports_to_slack()
