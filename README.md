# Kwantai Test Automation

**Note:** This project is on hold. For future reference, this test suite will be run by jenkins job with installed pytest and allure jenkins plugins to see allure report in jenkins.
#### Installation:
`pip install -r requirements.txt`

For Mas OS , allure framework installation is available via Homebrew to generate and serve allure report via command line.

`brew install allure`


#### Usages/Running Tests:
**Run all tests and generate allure report in local.**

`py.test --tb=short --disable-warnings --alluredir=var/results`

`allure serve var/results`

**Run all tests, post report in slack channel, without allure report.**

`py.test  -q -s -I Y --tb=short --disable-warnings > var/pytest_report.log`

**Run only marked regression_tests with the given keyword:**

`py.test  -q -s -I Y -m regression_tests --tb=short --disable-warnings > var/pytest_report.log`

**Other markers available to test specific feature tests.**

`is_receipt` - to run only isReceipt tests.

`retailer_test` - to run only retailers tests.

### Adding new tests:
For adding more retailers under `kwantai-test/src/tests/test_retailers.py` retailer test, please add retailer class name in the new line of `kwantai-test/retailers.txt` file.

For adding new model's feature (existing feature ex- is_receipt and retailer), please add new task python file under `kwantai-test/src/kwantai_test/tasks/`.

For adding tests for new feature, please add new test python file under `kwantai-test/src/tests/`

### Environment variables:

`API_URL` - Model endpoint

`SLACK_URL` - Slack webhook url

`S3_BUCKET` - Bucket name

`NON_RECEIPT_CLASSES` - List of non-receipt classes

`BUCKET_PREFIX` - Bucket prefix for `test_retailer` test data in `kwantai-test/src/tests/test_retailers.py`

`BUCKET_PREFIX_PURE_DATA` - Bucket prefix for `test_pure_sample` test data in `kwantai-test/src/tests/test_is_receipt.py`

`BUCKET_PREFIX_IMPURE_DATA` - Bucket prefix for `test_impure_sample` test data in `kwantai-test/src/tests/test_is_receipt.py`

`BUCKET_PREFIX_MIX_DATA_1_1` - Bucket prefix for `test_mix_sample_1_1` test data in `kwantai-test/src/tests/test_is_receipt.py`

`BUCKET_PREFIX_MIX_DATA_1_4` - Bucket prefix for `test_mix_sample_1_4` test data in `kwantai-test/src/tests/test_is_receipt.py`

`BUCKET_PREFIX_MIX_DATA_4_1` - Bucket prefix for `test_mix_sample_4_1` test data in `kwantai-test/src/tests/test_is_receipt.py`
