import pytest
import allure
import logging
from src.kwantai_test import accuracy, report
from src.kwantai_test.tasks.task_is_receipt import TaskIsReceipt
from src.kwantai_test import settings

logging.basicConfig(
    filename='var/logs/test_execution.log', level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(message)s'
)


@allure.severity(allure.severity_level.CRITICAL)
@allure.description("Testing model accuracy with pure data.")
@pytest.mark.regression_tests
@pytest.mark.is_receipt
def test_pure_sample():
    """This method tests model accuracy with pure data."""

    logging.info('Start executing test_pure_sample.')

    # Executing the task
    task_data = TaskIsReceipt(
        s3_bucket=settings.S3_BUCKET,
        bucket_prefix=settings.PURE_SAMPLE
    ).execute()

    # Calculating accuracy
    accuracy_result = accuracy.calc_positive_accuracy(
        total_counter=task_data['counter']["total"],
        positive_counter=task_data['counter']["positive"]
    )
    task_data['test_accuracy'] = accuracy_result

    # Generating report
    report.print_report(task_data, test_name=test_pure_sample)
    report.print_elapsed_metrics(task_data)
    report.print_receipt_objects(task_data)

    assert accuracy_result == 100

    logging.info('Finish executing test_pure_sample.')


@allure.severity(allure.severity_level.CRITICAL)
@allure.description("Testing model accuracy with impure data.")
@pytest.mark.regression_tests
@pytest.mark.is_receipt
def test_impure_sample():
    """This method tests model accuracy with impure data."""

    logging.info('Start executing test_impure_sample.')

    # Executing the task
    task_data = TaskIsReceipt(
        s3_bucket=settings.S3_BUCKET,
        bucket_prefix=settings.IMPURE_SAMPLE
    ).execute()

    # Calculating accuracy
    accuracy_result = accuracy.calc_negative_accuracy(
         total_counter=task_data['counter']["total"],
         negative_counter=task_data['counter']["negative"]

    )
    task_data['test_accuracy'] = accuracy_result

    # Generating report
    report.print_report(task_data, test_name='test_impure_sample')
    report.print_elapsed_metrics(task_data)
    report.print_non_receipt_objects(task_data)

    assert accuracy_result == 100

    logging.info('Finish executing test_impure_sample.')


@allure.severity(allure.severity_level.MINOR)
@allure.description("Testing model accuracy with mix of pure and impure data in 1:1 ratio.")
@pytest.mark.non_regression_tests
@pytest.mark.is_receipt_mix
def test_mix_sample_1_1():
    """This method tests model accuracy with mix of pure and impure data in 1:1 ratio."""

    logging.info('Start executing test_mix_sample_1_1.')

    # Executing the task
    task_data = TaskIsReceipt(
        s3_bucket=settings.S3_BUCKET,
        bucket_prefix=settings.MIX_SAMPLE_11
    ).execute()

    # Calculate pure data accuracy
    pure_accuracy = accuracy.calc_positive_accuracy(
        total_counter=task_data['counter']["total"],
        positive_counter=task_data['counter']["positive"]
    )

    # Calculate impure data accuracy
    impure_accuracy = accuracy.calc_negative_accuracy(
        total_counter=task_data['counter']["total"],
        negative_counter=task_data['counter']["negative"]
    )

    # Calculate total accuracy
    total_accuracy_result = pure_accuracy + impure_accuracy
    task_data['test_accuracy'] = total_accuracy_result

    # Generating report
    print(report.print_report(task_data, test_name=test_mix_sample_1_1))
    print(report.print_elapsed_metrics(task_data))

    assert pure_accuracy == 50
    assert impure_accuracy == 50

    logging.info('Finish executing test_mix_sample_1_1.')


@allure.severity(allure.severity_level.MINOR)
@allure.description("Testing model accuracy with mix of pure and impure data in 1:4 ratio.")
@pytest.mark.non_regression_tests
@pytest.mark.is_receipt_mix
def test_mix_sample_1_4():
    """This method tests model accuracy with mix of pure and impure data in 1:4 ratio."""

    logging.info('Start executing test_mix_sample_1_4.')

    # Executing the task
    task_data = TaskIsReceipt(
        s3_bucket=settings.S3_BUCKET,
        bucket_prefix=settings.MIX_SAMPLE_14
    ).execute()

    # Calculate pure data accuracy
    pure_accuracy = accuracy.calc_positive_accuracy(
        total_counter=task_data['counter']["total"],
        positive_counter=task_data['counter']["positive"]
    )

    # Calculate impure data accuracy
    impure_accuracy = accuracy.calc_negative_accuracy(
        total_counter=task_data['counter']["total"],
        negative_counter=task_data['counter']["negative"]
    )

    # Calculate total accuracy
    total_accuracy_result = pure_accuracy + impure_accuracy
    task_data['test_accuracy'] = total_accuracy_result

    # Generating report
    print(report.print_report(task_data, test_name=test_mix_sample_1_4))
    print(report.print_elapsed_metrics(task_data))

    assert pure_accuracy == 20
    assert impure_accuracy == 80

    logging.info('Finish executing test_mix_sample_1_4.')


@allure.severity(allure.severity_level.MINOR)
@allure.description("Testing model accuracy with mix of pure and impure data in 4:1 ratio.")
@pytest.mark.non_regression_tests
@pytest.mark.is_receipt_mix
def test_mix_sample_4_1():
    """This method tests model accuracy with mix of pure and impure data in 4:1 ratio."""

    logging.info('Start executing test_mix_sample_4_1.')

    # Executing the task
    task_data = TaskIsReceipt(
        s3_bucket=settings.S3_BUCKET,
        bucket_prefix=settings.MIX_SAMPLE_41
    ).execute()

    # Calculate pure data accuracy
    pure_accuracy = accuracy.calc_positive_accuracy(
        total_counter=task_data['counter']["total"],
        positive_counter=task_data['counter']["positive"]
    )

    # Calculate impure data accuracy
    impure_accuracy = accuracy.calc_negative_accuracy(
        total_counter=task_data['counter']["total"],
        negative_counter=task_data['counter']["negative"]
    )

    # Calculate total accuracy
    total_accuracy_result = pure_accuracy + impure_accuracy
    task_data['test_accuracy'] = total_accuracy_result

    # Generating report
    print(report.print_report(task_data, test_name=test_mix_sample_4_1))
    print(report.print_elapsed_metrics(task_data))

    assert pure_accuracy == 80
    assert impure_accuracy == 20

    logging.info('Finish executing test_mix_sample_4_1.')
