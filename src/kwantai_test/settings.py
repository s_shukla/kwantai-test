from environs import Env

env = Env()
env.read_env()

RETAILER_API_URL = env.str("API_URL")

SLACK_URL = env.url("SLACK_URL")
S3_BUCKET = env.str("S3_BUCKET")

NON_RECEIPT_CLASSES = env.list("NON_RECEIPT_CLASSES")

BUCKET_PREFIX = env.str("BUCKET_PREFIX")

PURE_SAMPLE = env.str("BUCKET_PREFIX_PURE_DATA")
IMPURE_SAMPLE = env.str("BUCKET_PREFIX_IMPURE_DATA")
MIX_SAMPLE_11 = env.str("BUCKET_PREFIX_MIX_DATA_1_1")
MIX_SAMPLE_14 = env.str("BUCKET_PREFIX_MIX_DATA_1_4")
MIX_SAMPLE_41 = env.str("BUCKET_PREFIX_MIX_DATA_4_1")
