aws_url = 'https://s3.console.aws.amazon.com/s3/buckets/analytics-lake-584921430927-us-east-1/'


def _print_sperataor(seperator_char: str = "=", count: int = 30):
    for x in range(0, count):
        print(seperator_char, end="")
    print(sep="\n")


def print_elapsed_metrics(report_data: dict):
    """This method generates elapsed time metrics report."""

    _print_sperataor()
    print(f"Fastest prediction in: {report_data['elapsed_metrics']['min']} sec")
    print(f"Slowest prediction in: {report_data['elapsed_metrics']['max']} sec")
    print(f"Average prediction in: {report_data['elapsed_metrics']['avg']} sec")
    _print_sperataor()


def print_report(report_data: dict, test_name):
    """This method generate test report for is_receipt model's tests."""
    print(f"Running test: {test_name}")
    print(f"Test sample size: {report_data['counter']['total']}")
    print(f"Model error count: {report_data['counter']['error']}")
    print(f"Receipt prediction(s): {report_data['counter']['positive']}")
    print(f"Non-receipt prediction(s): {report_data['counter']['negative']}")
    _print_sperataor()
    print(f"Model accuracy = {report_data['test_accuracy']}%")


def print_receipt_objects(report_data: dict):
    """This method prints S3 bucket object key(s) of images incorrectly predicted as non-receipt."""
    if [report_data['counter']['positive']] != [report_data['counter']['total']]:
        print("Receipt images incorrectly predicted as non-receipt images:")
        print(aws_url, *report_data['receipt_images'], sep="\n")


def print_non_receipt_objects(report_data: dict):
    """This method prints S3 bucket object key(s) of images incorrectly predicted as receipt."""
    if [report_data['counter']['negative']] != [report_data['counter']['total']]:
        print("Non-receipt images incorrectly predicted as receipt images:")
        print(aws_url, *report_data['non_receipt_images'], sep="\n")


def print_retailer_report(report_data: dict, retailer_count):
    """This method generate test report for retailer model's tests."""

    print(f"Model accuracy: {report_data['test_accuracy']}%")
    print(f"Test sample size: {report_data['counter']['total']}")
    print(f"Model error count: {report_data['counter']['error']}")
    print(f"Non-receipt prediction(s): {report_data['counter']['non_receipt']}")
    print(f"Correct prediction(s): {retailer_count}")
    incorrect_prediction_count = report_data['counter']['total'] - retailer_count
    print(f"Incorrect prediction(s): {incorrect_prediction_count}")

    if len(report_data["receipts_objects"]) > 0:
        print("Incorrectly predicted receipts:", sep="\n")
        _print_sperataor()
        for retailer_class, retailer_receipt_objects in report_data["receipts_objects"].items():
            print(f"- {retailer_class}", sep="\n")
            for receipt_object_key in retailer_receipt_objects:
                receipt_url = aws_url + receipt_object_key
                print(f"-- {receipt_url}", sep="\n")
    print_elapsed_metrics(report_data)
