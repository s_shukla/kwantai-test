from abc import ABC, abstractmethod
import requests
import json

import logging

logging.basicConfig(
        filename='var/logs/test_execution.log', level=logging.INFO,
        format='%(asctime)s:%(levelname)s:%(message)s'
    )


class KwantaiServiceError(ValueError):
    pass


class AbstractKwantai(ABC):
    """Abstract class for KwanTai Test Task"""

    def __init__(self):
        self._elapsed_time = []

    def _send_post(self, api_url: str, data=None, files=None):
        try:
            response = requests.post(api_url, data=data, files=files)
            self._elapsed_time.append(response.elapsed.total_seconds())

            return json.loads(response.text)
        except ValueError as error:
            KwantaiServiceError(error)

    @abstractmethod
    def is_receipt(self, receipt_image):
        pass

    def get_elapsed_metrics(self):
        return self._elapsed_time
