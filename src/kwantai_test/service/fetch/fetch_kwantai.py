from src.kwantai_test.service.fetch.base_kwantai import AbstractKwantai
from src.kwantai_test import settings
import logging

logging.basicConfig(
        filename='var/logs/test_execution.log', level=logging.INFO,
        format='%(asctime)s:%(levelname)s:%(message)s'
    )


class FetchKwantai(AbstractKwantai):

    def is_receipt(self, receipt_image):
        return self._send_post(
            api_url=settings.IS_RECEIPT_API_URL,
            files={"file": receipt_image}
        )

    def retailer(self, receipt_image):
        return self._send_post(
            api_url=settings.RETAILER_API_URL,
            files={"file": receipt_image}
        )
