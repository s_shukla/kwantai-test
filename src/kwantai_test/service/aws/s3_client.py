import boto3
import logging

logging.basicConfig(
        filename='var/logs/test_execution.log', level=logging.INFO,
        format='%(asctime)s:%(levelname)s:%(message)s'
    )


class FetchS3BucketError(ValueError):
    """Fetch AWS S3 general error class"""
    pass


def get_objects_keys(bucket: str, prefix: str = ''):
    """Returns list of objects' keys

    Args:
        bucket: Name of the S3 bucket
        prefix: Prefix of the bucket object

    Raises:
        FetchS3BucketError When bucket name is empty.
    """

    if not bucket:
        raise FetchS3BucketError("Bucket name is empty.")

    try:
        response = boto3.client('s3').list_objects_v2(Bucket=bucket, Prefix=prefix)
        logging.info('Getting object keys form S3 bucket.')

    except ValueError:
        raise FetchS3BucketError("Unexpected error occurred while connecting to S3.")

    if response is None:
        raise FetchS3BucketError("Empty response for list_objects_v2 call in FetchS3.get_objects_keys() method.")

    objects_keys_list = []
    for bucket_object in response['Contents']:
        if ("Size" in bucket_object) and (bucket_object['Size'] != 0):
            objects_keys_list.append(bucket_object['Key'])
    return objects_keys_list


def get_object(bucket: str, bucket_object_key: str):
    bucket_object = boto3.client('s3').get_object(Bucket=bucket, Key=bucket_object_key)
    if 'Body' not in bucket_object:
        raise FetchS3BucketError("'Body' key in bucket_object is none.")
    logging.info('Successfully received bucket object.')
    return bucket_object['Body'].read()

