
def calc_positive_accuracy(total_counter: int, positive_counter: int):
    """Calculate pure data accuracy."""

    return round((positive_counter / total_counter) * 100, 2)


def calc_negative_accuracy(total_counter: int, negative_counter: int):
    """Calculate impure data accuracy."""

    return round((negative_counter / total_counter) * 100, 2)
