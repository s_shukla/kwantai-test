from abc import ABC, abstractmethod


class AbstractTask(ABC):
    """Abstract class for KwanTai Test Task"""

    @abstractmethod
    def execute(self):
        """This method executes the task."""
        pass
