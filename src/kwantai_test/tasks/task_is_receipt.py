from src.kwantai_test.tasks.abstract_task import AbstractTask
from src.kwantai_test.service.aws import s3_client
from src.kwantai_test.service.fetch import (FetchKwantai, KwantaiServiceError)
from statistics import mean
from src.kwantai_test import settings
import logging


logging.basicConfig(
    filename='var/logs/test_execution.log',
    level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(message)s'
)


class TaskIsReceipt(AbstractTask):

    def __init__(self, s3_bucket: str, bucket_prefix: str = ''):
        self.s3_bucket = s3_bucket
        self.bucket_prefix = bucket_prefix
        self.total_counter = self.positive_counter = self.negative_counter = self.resp_err_counter = 0
        self.test_accuracy = 0
        self.receipt_objects = []
        self.non_receipt_objects = []

    def execute(self):
        """This method executes the task."""
        # Lists to store incorrect prediction S3 bucket directory

        # Get keys of objects of S3 bucket
        objects_keys_list = s3_client.get_objects_keys(
            bucket=self.s3_bucket,
            prefix=self.bucket_prefix
        )

        # Initializing the KwanTai ML model service
        model_service = FetchKwantai()

        # Iterating through each object key and getting that object
        logging.info('Starting iteration.')
        for object_key in objects_keys_list:
            # Download the object from S3 bucket
            bucket_object = s3_client.get_object(
                bucket=self.s3_bucket,
                bucket_object_key=object_key
            )

            try:
                response = model_service.retailer(receipt_image=bucket_object)
            except KwantaiServiceError:
                # Increment response error counter
                self.resp_err_counter = self.resp_err_counter + 1
            else:
                # Incrementing total counter
                self.total_counter = self.total_counter + 1

                # Check if the receipt was successfully predicted by ML model endpoint
                if "predicted_class" not in response:
                    # Increment response error counter
                    self.resp_err_counter = self.resp_err_counter + 1
                else:
                    if response["predicted_class"] in settings.NON_RECEIPT_CLASSES:
                        self.negative_counter = self.negative_counter + 1
                        self.non_receipt_objects.append(object_key)
                    else:
                        # if so, increment the is_receipt counter
                        self.positive_counter = self.positive_counter + 1
                        self.receipt_objects.append(object_key)

            logging.info(f"predicted_class for key {object_key} is {response['predicted_class']}")

        # elapsed time matrics
        elapsed_metrics = model_service.get_elapsed_metrics()
        elapsed_metrics.sort()

        return {
            "counter": {
                "total": self.total_counter,
                "positive": self.positive_counter,
                "negative": self.negative_counter,
                "error": self.resp_err_counter,
            },
            "elapsed_metrics": {
                "min": round(elapsed_metrics[0], 2),
                "max": round(elapsed_metrics[-1], 2),
                "avg": round(mean(elapsed_metrics), 2)
            },
            "receipt_images": self.non_receipt_objects,
            "non_receipt_images": self.receipt_objects
        }
