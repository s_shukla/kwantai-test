from src.kwantai_test.tasks.abstract_task import AbstractTask
from src.kwantai_test.service.aws import s3_client
from src.kwantai_test.service.fetch import (FetchKwantai, KwantaiServiceError)
from statistics import mean
from src.kwantai_test import settings
import logging


logging.basicConfig(
    filename='var/logs/test_execution.log', level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(message)s'
)


class TaskRetailer(AbstractTask):

    def __init__(self, s3_bucket: str, expected_retailer_key: str, bucket_prefix: str = ''):
        self.s3_bucket = s3_bucket
        self.bucket_prefix = bucket_prefix
        self.expected_retailer_key = expected_retailer_key
        self.total_count = self.negative_counter = self.resp_err_count = 0
        self.predicted_class_counter = {}
        self.receipts_objects = {}

    def execute(self):
        """This method executes the task."""

        # Get keys of objects of S3 bucket
        objects_keys_list = s3_client.get_objects_keys(
            bucket=self.s3_bucket,
            prefix=self.bucket_prefix
        )

        # Initializing the KwanTai ML model service
        model_service = FetchKwantai()

        # Iterating through each object key and getting that object
        logging.info('Starting iteration.')
        for object_key in objects_keys_list:
            # Download the object from S3 bucket
            bucket_object = s3_client.get_object(
                bucket=self.s3_bucket,
                bucket_object_key=object_key
            )
            try:
                response = model_service.retailer(receipt_image=bucket_object)
            except KwantaiServiceError:
                # Increment response error counter
                self.resp_err_count = self.resp_err_count + 1
            else:
                # Incrementing total counter
                self.total_count = self.total_count + 1

                # Check if the receipt was successfully predicted by ML model endpoint
                if "predicted_class" not in response:
                    # if so, increment the resp_err_count counter
                    self.resp_err_count = self.resp_err_count + 1
                else:
                    predicted_class = response["predicted_class"]
                    # Non receipts logic
                    if predicted_class in settings.NON_RECEIPT_CLASSES:
                        self.negative_counter = self.negative_counter + 1
                        self._set_receipts_objects("non_receipts", object_key)
                    else:
                        # Unexpected predicted receipt objects.
                        self._set_receipts_objects(predicted_class, object_key)
                        # Predicted classes counter
                        self._set_predicted_class_counter(predicted_class)

                logging.info(f"predicted_class for key {object_key} is {response['predicted_class']}")

        # elapsed time metrics
        elapsed_metrics = model_service.get_elapsed_metrics()
        elapsed_metrics.sort()

        return {
            "counter": {
                "total": self.total_count,
                "non_receipt": self.negative_counter,
                "error": self.resp_err_count,
                "predicted_class_counter": self.predicted_class_counter
            },
            "elapsed_metrics": {
                "min": round(elapsed_metrics[0], 2),
                "max": round(elapsed_metrics[-1], 2),
                "avg": round(mean(elapsed_metrics), 2)
            },
            "receipts_objects": self.receipts_objects
        }
    
    def _set_receipts_objects(self, predicted_class: str, object_key):
        """Receipts objects that were predicted other than the expected class"""

        if predicted_class != self.expected_retailer_key:
            if predicted_class not in self.receipts_objects:
                self.receipts_objects[predicted_class] = [object_key]
            else:
                self.receipts_objects[predicted_class].append(object_key)

    def _set_predicted_class_counter(self, predicted_class: str):
        """Predicted classes counter."""

        if predicted_class not in self.predicted_class_counter:
            self.predicted_class_counter[predicted_class] = 1
        else:
            self.predicted_class_counter[predicted_class] = self.predicted_class_counter[predicted_class] + 1
