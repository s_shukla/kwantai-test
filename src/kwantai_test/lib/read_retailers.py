def read_retailers():
    """This method reads retailers from 'retailers.txt' file and add them in the list 'retailers' list."""

    with open("retailers.txt") as retailers_file:
        retailers = []
        for retailer in retailers_file:
            retailers = retailers_file.read().splitlines()
        return retailers
