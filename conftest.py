from src.kwantai_test.lib.read_retailers import read_retailers
import pytest
import slack

# Initializing _read_retailers method.
retailer_keys = read_retailers()


@pytest.fixture(params=retailer_keys)
def retailer_key(request):
    return request.param


@pytest.fixture
def slack_integration_flag():
    """pytest fixture for os version"""

    return pytest.config.getoption("-I")


# command line options
def pytest_addoption(parser):
    parser.addoption(
        "-I",
        "--slack_integration_flag",
        dest="slack_integration_flag",
        default="N",
        help="Post the test report on slack channel: Y or N"
    )


# pytest plugin hook
def pytest_sessionfinish(session, exitstatus):
    "executes after whole test run finishes."
    if session.config.getoption("-I").lower() == 'y':
        slack.post_reports_to_slack()
